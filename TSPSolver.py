#!/usr/bin/python3

from which_pyqt import PYQT_VER
if PYQT_VER == 'PYQT5':
	from PyQt5.QtCore import QLineF, QPointF
elif PYQT_VER == 'PYQT4':
	from PyQt4.QtCore import QLineF, QPointF
else:
	raise Exception('Unsupported Version of PyQt: {}'.format(PYQT_VER))




import time
import numpy as np
from TSPClasses import *
import heapq
import itertools
import random


class TSPSolver:
	def __init__( self, gui_view ):
		self._scenario = None
		self.BSSF = math.inf

	def setupWithScenario( self, scenario ):
		self._scenario = scenario


	''' <summary>
		This is the entry point for the default solver
		which just finds a valid random tour.  Note this could be used to find your
		initial BSSF.
		</summary>
		<returns>results dictionary for GUI that contains three ints: cost of solution, 
		time spent to find solution, number of permutations tried during search, the 
		solution found, and three null values for fields not used for this 
		algorithm</returns> 
	'''

	def defaultRandomTour( self, time_allowance=60.0 ):
		results = {}
		cities = self._scenario.getCities()
		ncities = len(cities)
		foundTour = False
		count = 0
		bssf = None
		start_time = time.time()
		while not foundTour and time.time()-start_time < time_allowance:
			# create a random permutation
			perm = np.random.permutation( ncities )
			route = []
			# Now build the route using the random permutation
			for i in range( ncities ):
				route.append( cities[ perm[i] ] )
			bssf = TSPSolution(route)
			count += 1
			if bssf.cost < np.inf:
				# Found a valid route
				foundTour = True
		end_time = time.time()
		results['cost'] = bssf.cost if foundTour else math.inf
		results['time'] = end_time - start_time
		results['count'] = count
		results['soln'] = bssf
		results['max'] = None
		results['total'] = None
		results['pruned'] = None
		return results


	''' <summary>
		This is the entry point for the greedy solver, which you must implement for 
		the group project (but it is probably a good idea to just do it for the branch-and
		bound project as a way to get your feet wet).  Note this could be used to find your
		initial BSSF.
		</summary>
		<returns>results dictionary for GUI that contains three ints: cost of best solution, 
		time spent to find best solution, total number of solutions found, the best
		solution found, and three null values for fields not used for this 
		algorithm</returns> 
	'''

	# Finds the index of the city to which the given city has the smallest cost. Returns -1 if all routes are unreachable. Has a worst case time complexity of O(n), where n is the number of cities. Constant space complexity.
	def find_best_city_index(self, start_city, cities):
		best_index = -1
		smallest_cost = math.inf
		# Go through each city to see which ones we can get to
		for city_index, city in enumerate(cities):
			city_cost = start_city.costTo(city)
			# If it is cheaper to get to another city, then that is now the next best choice
			if city_cost < smallest_cost:
				best_index = city_index
				smallest_cost = city_cost
		return best_index

	# Time complexity is O(n^3). O(n) space complexity.
	def greedy( self,time_allowance=60.0 ):
		start_time = time.time()

		results = {}

		# Space complexity is O(n), where n is the number of cities
		potential_start_cities = self._scenario.getCities()[:]
		bssf = None
		
		foundTour = False
		count = 0

		# If we have used up our allotted time allowance, then quit with the results that we have. Otherwise, continue until we find a tour.
		# This loop, in worse case scenario, runs n times. With the operations within it, the total time complexity becomes O(n^3). O(n) space complexity.
		while len(potential_start_cities) > 0 and not foundTour and time.time() - start_time < time_allowance:
			# Get a copy of the cities. Space complexity is O(V), where V is the number of cities.
			cities = self._scenario.getCities()[:]

			# Create the route list
			route = []

			# Choose the last city not yet used as a starting place. Constant space and time complexity
			start_city = potential_start_cities.pop()

			# Add that city to the route
			route.append(start_city)

			# Take that city out of the list of cities on the routes
			cities.pop(len(potential_start_cities))

			# While we still have cities left to go through, find the next route by getting the cheapest route.
			# This loop runs n times. With the operations within it, the total time complexity becomes O(n^2). Constant space complexity.
			while len(cities) > 0:
				# Choose to go to the next city that has the smallest cost (but not to itself). Constant space complexity, O(n) time complexity.
				next_city_index = self.find_best_city_index(start_city, cities)

				# If no city is connected to this city, then give up on this iteration, it isn't possible
				if next_city_index == -1:
					break

				# Make that city the next start city
				start_city = cities.pop(next_city_index)

				# Add that city to the route list. This is constant space complexity because we already created the city, we are just moving the reference to the route list
				route.append(start_city)
			
			# If we broke out early because it wasn't possible to go through all the points, then try starting from another city
			if len(cities) > 0:
				continue

			# Create the bssf solution. Time complexity of O(n), because we have to calculate the cost from each city to the next city in the route (and there are n cities in the route)
			bssf = TSPSolution(route)

			# Add to the count of intermediate solutions considered
			count += 1

			# If it is a viable route (i.e. the last city can get back to the first city), then we are done
			if bssf.cost < np.inf:
				foundTour = True

		end_time = time.time()

		results['cost'] = bssf.cost if foundTour else math.inf
		results['time'] = end_time - start_time
		results['count'] = count
		results['soln'] = bssf
		results['max'] = None
		results['total'] = None
		results['pruned'] = None
		
		return results

	# Total time complexity of O(n^2). Constant space complexity
	def reduceMatrix(self, matrix):
		cost = 0

		matrix_shape = matrix.shape

		# This loop runs n times. Total time complexity of O(n^2)
		for row_index in range(matrix_shape[0]):

			# Find the smallest value in that row, and the column index for that value
			smallest_value = math.inf
			smallest_value_index = -1

			# This loop runs n times
			for column_index in range(matrix_shape[1]):
				if matrix[row_index][column_index] < smallest_value:
					smallest_value = matrix[row_index][column_index]
					smallest_value_index = column_index

			# If we found anything smaller than infinity, then reduce everything in that row by that smallest value, and add that value to the cost
			if smallest_value_index != -1:
				cost += smallest_value
				for column_index in range(matrix_shape[1]):
					matrix[row_index][column_index] -= smallest_value
					
		# This loop runs n times. Total time complexity of O(n^2)
		for column_index in range(matrix_shape[1]):
		
			# Find the smallest value in that column, and the row index for that value
			smallest_value = math.inf
			smallest_value_index = -1

			# This loop runs n times
			for row_index in range(matrix_shape[0]):
				if matrix[row_index][column_index] < smallest_value:
					smallest_value = matrix[row_index][column_index]
					smallest_value_index = row_index

			# If we found anything smaller than infinity, then reduce everything in that column by that smallest value, and add that value to the cost
			if smallest_value_index != -1:
				cost += smallest_value
				for row_index in range(matrix_shape[0]):
					matrix[row_index][column_index] -= smallest_value

		return cost

	# Time and space complexity of O(n^2).
	def find_initial_cost_matrix(self, cities):
		# Space complexity of O(n^2). The time complexity is actually constant, since numpy doesn't touch the values in the array, like it would with .zeros
		matrix = np.empty([len(cities), len(cities)])
		cost = 0

		# Calculate the costs to and from every city. The two for loops has a time complexity of O(n^2)
		for row_city in cities:
			for column_city in cities:
				matrix[row_city._index][column_city._index] = row_city.costTo(column_city)

		# Reduce the matrix. Time complexity of O(n^2)
		cost += self.reduceMatrix(matrix)

		return matrix, cost

	# Time and space complexity of O(n^2).
	def calculate_cost_and_matrix(self, child_node_city, parent_node_city, parent_matrix, parent_cost):
		# Space complexity of O(n^2)
		reduced_matrix = np.copy(parent_matrix)
		cost = parent_cost

		matrix_shape = reduced_matrix.shape

		# Add any extra cost if getting to this node wasn't accounted for in the original cost estimate. Constant time and space complexity
		cost += reduced_matrix[parent_node_city._index][child_node_city._index]

		# Put infinities in the correct spots in the matrix. This loop has a time complexity of O(n)
		for index in range(matrix_shape[0]):
			# Put infinity values in the column for the city we are calculating the matrix for
			reduced_matrix[index][child_node_city._index] = math.inf

			# Put infinity values in the row for the city we are calculating the matrix for
			reduced_matrix[parent_node_city._index][index] = math.inf

		# Put infinity to get to the node you got from, since you can't do that
		reduced_matrix[child_node_city._index][parent_node_city._index] = math.inf

		# Reduce the matrix. Time complexity of O(n^2)
		cost += self.reduceMatrix(reduced_matrix)

		return reduced_matrix, cost

	''' <summary>
		This is the entry point for the branch-and-bound algorithm that you will implement
		</summary>
		<returns>results dictionary for GUI that contains three ints: cost of best solution, 
		time spent to find best solution, total number solutions found during search (does
		not include the initial BSSF), the best solution found, and three more ints: 
		max queue size, total number of states created, and number of pruned states.</returns> 
	'''

	def branchAndBound( self, time_allowance=60.0 ):
		start_time = time.time()
		
		results = {}

		# Constant time and space complexity, since we are just getting a pointer to the list of cities
		cities = self._scenario.getCities()
		number_of_cities = len(cities)
		bssf = None

		# Find an initial solution using the greedy algorithm to find a decent bssf. As noted in the method, this method has a time complexity is O(n^3). O(n) space complexity.
		greedy_solution = self.greedy(5);
		
		if greedy_solution['cost'] < math.inf:
			bssf = greedy_solution['soln']
		
		# Start at one because we will always add at least the first city
		max_reached_size_of_queue = 1
		total_number_of_nodes_created = 1
		count = 0
		pruned_nodes = 0

		# Initialize the priority queue. Constant time and space complexity
		priority_queue = HeapPriorityQueue()

		# Just start at the first city. Calculate the initial reduced cost matrix and its cost. Time and space complexity of O(n^2).
		initial_cost_matrix, initial_cost = self.find_initial_cost_matrix(cities)
		route = [cities[0]]

		# Space complexity of O(n).
		remaining_cities = cities[1:]

		# Constant time and space complexity, since we are just giving the fields of this new object the references to what we already created.
		start_node = branchNode(route[0], initial_cost_matrix, initial_cost, route, remaining_cities)

		# Add that node to the priority queue. Constant time and space complexity, since the queue is empty.
		priority_queue.insert(start_node)

		# If we have used up our allotted time allowance, then quit with the results that we have. Otherwise, continue looking until we have parsed or pruned the whole tree.
		# In worst case scenario, we will have to go through every possible node in the tree. This is O(n!) time complexity.
		while not priority_queue.is_empty() and time.time() - start_time < time_allowance:
			
			# Pull out the highest priority item from the queue. O(logV) time complexity, where V is the number of nodes in the queue. 
			branch_node = priority_queue.delete_min()

			# Check to make sure that the branch is still better than the bssf (the bssf could have been updated while the node was in the queue). Constant time and space complexity
			if bssf != None and bssf.cost <= branch_node.cost:
				pruned_nodes += 1
				continue

			# If there are no more remaining cities to go through, then see if the route we have is a viable solution
			if len(branch_node.remaining_cities) == 0:
				# Time complexity of O(n)
				potential_solution = TSPSolution(branch_node.route)
				# If that solution is viable and better than the current bssf, then update the bssf and move onto the next node
				if bssf == None or potential_solution.cost < np.inf and bssf.cost > potential_solution.cost:
					bssf = potential_solution
					count += 1
				continue

			# Go through each of the possible children nodes of that item. 
			# This loops can run up to n - 1 times (although most nodes will run far less times, since they will have fewer cities remaining to be processed). Time and space complexity of O(n^3)
			for index, potential_child_node_city in enumerate(branch_node.remaining_cities):

				# If you can't even get to this child city from the current branch node, then don't process this node any further
				if branch_node.matrix[branch_node.city._index][potential_child_node_city._index] == math.inf:
					continue

				# Calculate the reduced matrix and lower bound cost for that possible child node. Time and space complexity of O(n^2).
				reduced_matrix, cost = self.calculate_cost_and_matrix(potential_child_node_city, branch_node.city, branch_node.matrix, branch_node.cost)

				# If that estimated cost is smaller than the current solution cost, then create a node from that information and add it to the priority queue
				if bssf == None or bssf.cost > cost:
					# Create the route list for that node. The combined route and remaining cities will have n size.
					new_route_for_node = branch_node.route[:]
					new_route_for_node.append(potential_child_node_city)

					# Calculate how many cities are remaining to be passed through for that node
					remaining_cities_for_node = branch_node.remaining_cities[:]
					remaining_cities_for_node.pop(index)

					# Create the actual node.
					new_child_node = branchNode(potential_child_node_city, reduced_matrix, cost, new_route_for_node, remaining_cities_for_node)

					# Insert the node into the priority queue. O(logV) time complexity
					priority_queue.insert(new_child_node)

					# Keep track of how many items we added to the queue, as well as the max size reached
					total_number_of_nodes_created += 1
					if priority_queue.size() > max_reached_size_of_queue:
						max_reached_size_of_queue = priority_queue.size()
				else:
					pruned_nodes += 1

		end_time = time.time()

		results['cost'] = bssf.cost if count > 0 or greedy_solution['cost'] < math.inf else math.inf
		results['time'] = end_time - start_time
		results['count'] = count
		results['soln'] = bssf
		results['max'] = max_reached_size_of_queue
		results['total'] = total_number_of_nodes_created
		results['pruned'] = pruned_nodes

		return results

	''' <summary>
		This is the entry point for the algorithm you'll write for your group project.
		</summary>
		<returns>results dictionary for GUI that contains three ints: cost of best solution, 
		time spent to find best solution, total number of solutions found during search, the 
		best solution found.  You may use the other three field however you like.
		algorithm</returns> 
	'''
	

	def fancy( self,time_allowance=60.0 ):
		start_time = time.time()
		
		results = {}

		# Constant time and space complexity, since we are just getting a pointer to the list of cities
		cities = self._scenario.getCities()
		number_of_cities = len(cities)

		temperature = 1000 * number_of_cities
		temperature_threshold = 25;
		TEMPERATURE_REDUCTION = 0.8
		PERMUTATION_ATTEMPTS = 100
		NUMBER_OF_SWAPS = 3
		bssf = None
		current_solution = None

		count = 0

		# Find an initial solution using the greedy algorithm to find a decent bssf. As noted in the method, this method has a time complexity is O(n^3). O(n) space complexity.
		greedy_solution = self.greedy(5);
		
		# Use the greedy solution if there is one. Otherwise, choose a random tour
		if greedy_solution['cost'] < math.inf:
			print("\nGreedy solution cost: " + str(greedy_solution['cost']))
			bssf = greedy_solution['soln']
			current_solution = greedy_solution['soln']
		else:
			random_solution = self.defaultRandomTour(5)
			if random_solution['cost'] < math.inf:
				bssf = random_solution['soln']
				current_solution = random_solution['soln']

		loop_count = 0
		while temperature > temperature_threshold and time.time() - start_time < time_allowance:
			potential_solution = self.permutateSolution(current_solution, PERMUTATION_ATTEMPTS, NUMBER_OF_SWAPS)

			if self.shouldAcceptCost(potential_solution.cost, current_solution.cost, temperature):
				count += 1
				current_solution = potential_solution
				if bssf.cost > potential_solution.cost:
					#print("\nNew solution found with cost of " + str(potential_solution.cost))
					bssf = potential_solution

			loop_count += 1
			if loop_count % 100 == 0:
				loop_count = 0
				temperature *= TEMPERATURE_REDUCTION



		end_time = time.time()

		results['cost'] = bssf.cost if bssf != None else math.inf
		results['time'] = end_time - start_time
		results['count'] = count
		results['soln'] = bssf
		results['max'] = 0
		results['total'] = 0
		results['pruned'] = 0

		return results

	def shouldAcceptCost(self, potential_cost, current_cost, temperature):
		if potential_cost == math.inf:
			return False

		# If the new cost is better than the old cost, then accept it
		if potential_cost < current_cost:
			return True

		# Otherwise, calculate an acceptance probability
		exponent_value = (current_cost - potential_cost) / temperature
		#print("\nCurrent cost: " + str(current_cost))
		#print("\nPotential cost: " + str(potential_cost))
		#print("\nCurrent temperature: " + str(temperature))
		#print("\nInitial exponent value: " + str(exponent_value))
		acceptance_probability = np.exp(exponent_value)

		acceptance_threshold = random.uniform(0, 1)

		#print("\nAcceptance probability: " + str(acceptance_probability))
		#print("\nAcceptance threshold: " + str(acceptance_threshold))


		if acceptance_threshold >= acceptance_probability:
			return False
		else:
			#print("\nTHIS IS A GOOD SOLUTION WITH VALUE " + str(potential_cost) + " COMPARED TO INITIAL COST OF " + str(current_cost) + ", ACCEPT!!")
			return True

	def permutateSolution(self, solution, attempts, number_of_swaps):
		best_found_solution = None
		potential_solution = None
		for i in range(attempts):
			route = solution.route[:]

			#route_size = len(route)
			#index1, index2 = random.sample(range(route_size // 2), 2)
			#index2 += route_size // 2
			#reversed_section = route[index1:index2][:]
			#reversed_section.reverse()
			#route = route[0:index1] + reversed_section + route[index2:route_size]

			
			#route_size = range(len(route))
			#indexes = random.sample(route_size, number_of_swaps)
			#route[indexes[0]], route[indexes[1]] = route[indexes[1]], route[indexes[0]]
			

			route_size = range(len(route))
			indexes = random.sample(route_size, number_of_swaps)
			swap_indexes = indexes[:]
			swapping_index = swap_indexes.pop(0)
			swap_indexes.append(swapping_index)
			temp_route = route[:]

			#print("\nIndexes to be swapped")
			#print(indexes)
			#print(swap_indexes)

			for index in range(len(indexes)):
				temp_route[indexes[index]] = route[swap_indexes[index]]

			route = temp_route


			#print("\nNew route")
			#print([city._name for city in route])
			#print([city._name for city in solution.route])
			
			potential_solution = TSPSolution(route)

			if (potential_solution.cost < np.inf and best_found_solution == None ) or (best_found_solution != None and best_found_solution.cost > potential_solution.cost):
				#print("\nFound better permutation")
				best_found_solution = potential_solution

		return best_found_solution

		
# Total space complexity for each node will be O(n^2)
class branchNode:
	def __init__(self, city, matrix, cost, route, remaining_cities):
		# Constant space complexity
		self.city = city
		# O(n^2) space complexity
		self.matrix = matrix
		self.cost = cost
		# Combined route and remaining cities have a space complexity of O(n)
		self.route = route
		self.remaining_cities = remaining_cities
		self.indexer = -1

class HeapPriorityQueue:
	# Constant time complexity and space complexity
	def __init__(self):
		self.branch_node = []
		self.node_indexes = []
	
	# Constant time complexity
	def size(self):
		return len(self.branch_node)

	# Constant time complexity
	def is_empty(self):
		return (len(self.branch_node) == 0)

	# The time complexity of O(logV), where V is the number of points in the heap. The space complexity is constant (because the nodes aren't created here, but are only passed in).
	def insert(self, node):
		# Construct the node pair. Constant time and space complexity.
		node_pair = [node]

		# Add the node to the heap. Constant time and space complexity. 
		insert_index = len(self.branch_node)
		node_pair[0].indexer = len(self.node_indexes)
		self.branch_node.append(node_pair)
		self.node_indexes.append(insert_index)
		
		# Bubble the node up the heap to its correct location. This is O(logV) operations, as noted in the method
		self._bubble_up(node_pair)

	# The time complexity of O(logV), where V is the number of points in the heap. The space complexity is constant.
	def delete_min(self):
		# Get the min element from the tree
		min_node = self.branch_node[0]

		# If it is the last element, then just return it and pop it from the tree.
		if len(self.branch_node) == 1:
			self.branch_node.pop()
			return min_node[0]

		# Get the node at the end of the heap. Constant time and space complexity.
		node_pair = self.branch_node[-1]

		# Stick that node as the new root of the heap. Constant time and space complexity.
		self.branch_node[0] = node_pair 

		# Mark the new index of that node. Constant time and space complexity.
		self.node_indexes[node_pair[0].indexer] = 0

		# Mark the old root's index as nothing, just in case. Constant time and space complexity.
		self.node_indexes[min_node[0].indexer] = None

		# Pop off the extra node slot. Constant time complexity.
		self.branch_node.pop()

		# Bubble down that node we moved to the root. This is O(logV) operations, as noted in the method
		self._bubble_down(node_pair)

		# Return the min element of the queue
		return min_node[0]
	
	# We only go up from the bottom of the tree up to the root of the tree, one layer each time iteration, for a total of log(V) times in the worst case. So, the time complexity is O(logV), where V is the number of nodes in the heap 
	def _bubble_up(self, node_pair):
		# Get the parent node. Constant time and space complexity.
		parent_node_pair = self._get_parent(node_pair[0])
		
		# If the node has a value equal to or larger than its parent, or has no parent, then stop bubbling up. Constant time and space complexity.
		if parent_node_pair == None:
			return

		if node_pair[0].cost >= parent_node_pair[0].cost:
			return

		# Otherwise, switch the two. Switch the index array to match the new placement. Constant time and space complexity.
		parent_node_index = parent_node_pair[0].indexer
		node_index = node_pair[0].indexer
		temp_index = self.node_indexes[parent_node_index]

		self.node_indexes[parent_node_index] = self.node_indexes[node_index]
		self.node_indexes[node_index] = temp_index

		# Switch the actual name value pairs. Constant time and space complexity.
		temp_node_pair = parent_node_pair
		self.branch_node[self.node_indexes[node_pair[0].indexer]] = node_pair
		self.branch_node[self.node_indexes[temp_node_pair[0].indexer]] = temp_node_pair


		# Continue to bubble up
		self._bubble_up(node_pair)

	# We only go down from the root to the bottom of the tree, going down one layer each time, for a total of log(V) times in the worst case. So, the time complexity is O(logV), where V is the number of nodes in the heap 
	def _bubble_down(self, node_pair):
		# Get the children nodes. Constant time and space complexity.
		children_node_pairs = self._get_children(node_pair[0])

		# If it has no children, then stop bubbling. Constant time and space complexity.
		if children_node_pairs[0] == None and children_node_pairs[1] == None:
			return

		# Otherwise, find which of the children nodes has a smaller value. Constant time and space complexity.
		smaller_child_node_pair = None
		if children_node_pairs[0] == None and children_node_pairs[1] != None:
			smaller_child_node_pair = children_node_pairs[1]
		if children_node_pairs[0] != None and children_node_pairs[1] == None:
			smaller_child_node_pair = children_node_pairs[0]
		elif children_node_pairs[0][0].cost == math.inf:
			smaller_child_node_pair = children_node_pairs[1]
		elif children_node_pairs[1][0].cost == math.inf:
			smaller_child_node_pair = children_node_pairs[0]
		elif children_node_pairs[0][0].cost > children_node_pairs[1][0].cost:
			smaller_child_node_pair = children_node_pairs[1]
		else:
			smaller_child_node_pair = children_node_pairs[0]

		# If that node has a equal or larger value than the current node, then stop bubbling down. Constant time and space complexity.
		if node_pair[0].cost == math.inf and smaller_child_node_pair != math.inf:
			pass
		elif smaller_child_node_pair[0].cost == math.inf or smaller_child_node_pair[0].cost >= node_pair[0].cost:
			return

		# Otherwise, switch them. Constant time and space complexity.
		child_node_index = smaller_child_node_pair[0].indexer
		node_index = node_pair[0].indexer
		temp_value_index = self.node_indexes[child_node_index]

		self.node_indexes[child_node_index] = self.node_indexes[node_index]
		self.node_indexes[node_index] = temp_value_index

		# Switch the actual name value pairs. Constant time and space complexity.
		temp_node_pair = smaller_child_node_pair
		self.branch_node[temp_value_index] = node_pair
		self.branch_node[self.node_indexes[temp_node_pair[0].indexer]] = temp_node_pair

		# Continue to bubble down
		self._bubble_down(node_pair)

	# Gets the parent node of the given node, if there is any. Constant time and space complexity.
	def _get_parent(self, node):
		node_index = self.node_indexes[node.indexer]
		# If we are at the root, there is no parent
		if node_index == 0:
			return None

		parent_index = (node_index - 1) // 2
		return (self.branch_node[parent_index])
	
	# Gets the children node of the given node, if any. Constant time and space complexity
	def _get_children(self, node):
		node_index = self.node_indexes[node.indexer]
		first_child_index = (node_index * 2) + 1
		second_child_index = (node_index * 2) + 2

		first_child = None
		second_child = None

		number_nodes = len(self.branch_node)
		if number_nodes > first_child_index:
			first_child = self.branch_node[first_child_index]
		if number_nodes > second_child_index:
			second_child = self.branch_node[second_child_index]
		return [first_child, second_child]